package br.edu.ifsc.crud;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText etCodigo;
    private EditText etNomeUC;
    private EditText etNota;
    private SQLiteDatabase banco;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etCodigo = (EditText) findViewById(R.id.editTextCodigo);
        etNomeUC = (EditText) findViewById(R.id.editTextUC);
        etNota = (EditText) findViewById(R.id.editNota);
        banco = this.openOrCreateDatabase("banco", getBaseContext().MODE_PRIVATE,null);
        banco.execSQL("CREATE TABLE IF NOT EXISTS notas " +"(_id INTEGER PRIMARY KEY AUTOINCREMENT," +"nome_UC TEXT NOT NULL," +
                " " +"nota DECIMAL NOT NULL)" );
    }
    public void bntIncluirOnCLick(View view){
        ContentValues registro = new ContentValues();
        registro.put("nome_uc",etNomeUC.getText().toString());
        registro.put("nota", Double.parseDouble(etNota.getText().toString()));
        banco.insert("notas", null,registro);
        Toast.makeText(this,"Registro inserido",Toast.LENGTH_LONG).show();
    }
    public void bntAlterarOnClick(View view){
        int id = Integer.parseInt(etCodigo.getText().toString());
        ContentValues registro = new ContentValues();
        registro.put("nome_UC", etNomeUC.getText().toString());
        registro.put("nota", etNota.getText().toString());
        int i=banco.update("notas" , registro, "_id="+id, null);

        Toast.makeText(this,"Registro alterado com sucesso",Toast.LENGTH_LONG).show();
    }
    public void bntExcluirOnClick(View view){
        int id = Integer.parseInt(etCodigo.getText().toString());
        int i= banco.delete("notas","_id = "+id , null);
        Toast.makeText(this,"Registro Excluido",Toast.LENGTH_LONG).show();
        this.etCodigo.setText("");
        this.etNomeUC.setText("");
        this.etNota.setText("");
    }
    public void bntPesquisarOnClick(View view){
        final EditText edCodigoDialog = new EditText(this);
        AlertDialog.Builder telaPesquisa = new AlertDialog.Builder(this);
        DialogInterface.OnClickListener o = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                realizarPesquisa(Integer.parseInt(edCodigoDialog.getText().toString()));
            }
        };
        telaPesquisa.setTitle("Pesquisa");
        telaPesquisa.setIcon(android.R.drawable.ic_menu_search);
        telaPesquisa.setNegativeButton("Cancelar",null);
        telaPesquisa.setPositiveButton("OK",o);
        telaPesquisa.setView(edCodigoDialog);
        telaPesquisa.show();
    }
    public void realizarPesquisa(int id){
        Cursor registro =banco.query("notas",null,"_id="+id,null,null,null,null,null);

            String nome = registro.getString(registro.getColumnIndex("nome"));
            double nota = registro.getDouble(registro.getColumnIndex("nota"));

           this.etNota.setText(Double.toString(nota));
            this.etNomeUC.setText(nome);
            this.etCodigo.setText(Integer.toString(id));
        if(registro.moveToNext()){
            Toast.makeText(this,"Registro não encontrado",Toast.LENGTH_LONG).show();
        }
    }
    public void bntListarOnClick(View view){

    }

}
